﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Test.Models;
using Test.Models.Requests;

namespace Test.DataAccess
{
    public class UserDa
    {
        private readonly string _connectionString;

        public UserDa(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        /// <summary>
        /// Inserta usario
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public async Task<User> Insert(RequestInsertUser reqUser)
        {
            try
            {
                using (SqlConnection sql = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SP_USER_INSERT", sql))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("UserName", reqUser.UserName));
                        cmd.Parameters.Add(new SqlParameter("UserPassword", reqUser.UserPassword));
                        cmd.Parameters.Add(new SqlParameter("Name", reqUser.Name));
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;
                        await sql.OpenAsync();
                        cmd.ExecuteNonQuery();


                        User User = new User();
                        User.Id = (int)returnParameter.Value;
                        User.Name = reqUser.Name;
                        User.UserName = reqUser.UserName;
                        User.UserPassword = "******";
                        return User;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// se logea usuario
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public async Task<User> Login(RequestLogin user)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SP_USER_LOGIN", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("UserName", user.UserName));
                    cmd.Parameters.Add(new SqlParameter("UserPassword", user.UserPassword));
                    var response = new User();
                    await sql.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response = MapToValue(reader);
                            break;
                        }
                    }

                    return response;
                }
            }
        }

        private User MapToValue(SqlDataReader reader)
        {
            return new User()
            {
                Id = (Int64)reader["Id"],
                UserName = reader["UserName"].ToString(),
                UserPassword = "******",
                Name = reader["Name"].ToString(),
                DateCreation = (DateTime)reader["DateCreation"]
            };
        }
    }
}
