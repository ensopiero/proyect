﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Test.Models;
using Test.Models.Requests;

namespace Test.DataAccess
{
    public class TaskDa
    {
        private readonly string _connectionString;

        public TaskDa(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        /// <summary>
        /// Inserta usario
        /// </summary>
        /// <param name="Task"></param>
        /// <returns></returns>
        public async Task<RequestInsertTask> Insert(RequestInsertTask reqTask)
        {
            try
            {
                using (SqlConnection sql = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SP_TASK_INSERT", sql))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("Id_User", reqTask.IdUser));
                        cmd.Parameters.Add(new SqlParameter("Id_State", reqTask.IdState));
                        cmd.Parameters.Add(new SqlParameter("Description", reqTask.Description));
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;
                        await sql.OpenAsync();
                        cmd.ExecuteNonQuery();
                        reqTask.Id = (int)returnParameter.Value;


                        return reqTask;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Tasks>> Get(RequestSelectTask req)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SP_TASK_SELECT", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("Id", req.Id));
                    cmd.Parameters.Add(new SqlParameter("Id_User", req.IdUser));
                    cmd.Parameters.Add(new SqlParameter("Id_State", req.IdState));
                    cmd.Parameters.Add(new SqlParameter("description", req.Description));
                    var response = new List<Tasks>();
                    await sql.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response.Add(MapToValue(reader));
                        }
                    }

                    return response;
                }
            }
        }

        public async Task<Int64> Update(Int64 id)
        {
            try
            {
                using (SqlConnection sql = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SP_TASK_UPDATE", sql))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("Id", id));
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;
                        await sql.OpenAsync();
                        cmd.ExecuteNonQuery();
                        return (int)returnParameter.Value; 
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private Tasks MapToValue(SqlDataReader reader)
        {
            Tasks t = new Tasks();
            t.Id = (Int64)reader["Id"];
            t.Description = reader["Description"].ToString();
            t.DateCreation = (DateTime)reader["DateCreation"];
            t.User = new User();
            t.User.Id = (Int64)reader["Fk_Id_User"];
            t.User.UserName = reader["UserName"].ToString();
            t.User.Name = reader["Name"].ToString();
            t.State = new State();
            t.State.Id = (int)reader["Fk_Id_State"];
            t.State.Description = reader["StateDescription"].ToString();
            return t;
        }
    }
}
