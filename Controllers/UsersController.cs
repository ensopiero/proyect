﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Test.DataAccess;
using Test.Enums;
using Test.Models;
using Test.Models.Requests;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserDa _UserDa;
        private readonly IConfiguration _configuration;

        public UserController(UserDa userDa, IConfiguration configuration)
        {
            this._UserDa = userDa ?? throw new ArgumentNullException(nameof(userDa));
            this._configuration = configuration;
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<ResponseBE<User>> Insert(RequestInsertUser user)
        {
            ResponseBE<User> response = new ResponseBE<User>();
            try
            {
                User value = await _UserDa.Insert(user);
                if (value == null || value.Id <= 0 )
                {
                    response.Mensaje = "No se pudo ingresar el usuario";
                    response.Codigo = ResponseEnum.ERROR;
                    response.IdOperacion = "-1";
                }

                else
                {
                    response.Data = value;
                    response.Codigo = ResponseEnum.OK;
                    response.Mensaje = "OK";
                }

            }
            catch (Exception ex)
            {
                response.Codigo = ResponseEnum.FATAL;
                response.Mensaje = ex.Message;
            }

            return response;
        }

        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<ResponseBE<User>> Login(RequestLogin user)
        {
            ResponseBE<User> response = new ResponseBE<User>();
            try
            {
                User value = await _UserDa.Login(user);
                if (value == null || value.Id == 0)
                {
                    response.Mensaje = "Usuario y/o Clave incorrectas";
                    response.Codigo = ResponseEnum.ERROR;
                    response.IdOperacion = "-1";
                }

                else
                {

                    string token = GenerarTokenJWT(value);
                    value.Token = token;
                    response.Data = value;
                    response.Codigo = ResponseEnum.OK;
                    response.Mensaje = "OK";
                }

            }
            catch (Exception ex)
            {
                response.Codigo = ResponseEnum.FATAL;
                response.Mensaje = ex.Message;
            }

            return response;
        }


        // GENERAMOS EL TOKEN CON LA INFORMACIÓN DEL USUARIO
        private string GenerarTokenJWT(User usuarioInfo)
        {
            // CREAMOS EL HEADER //
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["JWT:ClaveSecreta"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

            // CREAMOS LOS CLAIMS //
            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, usuarioInfo.Id.ToString()),
                new Claim("User", usuarioInfo.UserName)
            };

            // CREAMOS EL PAYLOAD //
            var _Payload = new JwtPayload(
                    issuer: _configuration["JWT:Issuer"],
                    audience: _configuration["JWT:Audience"],
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    // Exipra a la 24 horas.
                    expires: DateTime.UtcNow.AddHours(24)
                );

            // GENERAMOS EL TOKEN //
            var _Token = new JwtSecurityToken(
                    _Header,
                    _Payload
                );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }
    }
}
