﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Test.DataAccess;
using Test.Enums;
using Test.Models;
using Test.Models.Requests;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskDa _TaskDa;
        private readonly IConfiguration _configuration;

        public TaskController(TaskDa taskDa, IConfiguration configuration)
        {
            this._TaskDa = taskDa ?? throw new ArgumentNullException(nameof(taskDa));
            this._configuration = configuration;
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<ResponseBE<RequestInsertTask>> Insert(RequestInsertTask reqTask)
        {
            ResponseBE<RequestInsertTask> response = new ResponseBE<RequestInsertTask>();
            try
            {
                RequestInsertTask value = await _TaskDa.Insert(reqTask);
                if (value == null || value.Id <= 0)
                {
                    response.Mensaje = "No se pudo ingresar la tarea";
                    response.Codigo = ResponseEnum.ERROR;
                    response.IdOperacion = "-1";
                }

                else
                {
                    response.Data = value;
                    response.Codigo = ResponseEnum.OK;
                    response.Mensaje = "OK";
                }

            }
            catch (Exception ex)
            {
                response.Codigo = ResponseEnum.FATAL;
                response.Mensaje = ex.Message;
            }

            return response;
        }

        [HttpGet]
        // [Authorize]
        [Route("Get")]
        public async Task<ResponseBE<List<Tasks>>> Get(Int64? id, Int64? idUser, int? idState,string description)
        { 
            ResponseBE<List<Tasks>> response = new ResponseBE<List<Tasks>>();
            try
            {
                RequestSelectTask req = new RequestSelectTask();
                req.Id = (id == null) ? 0 : (Int64)id;
                req.IdUser = (idUser == null) ? 0 : (Int64)idUser;
                req.IdState = (idState == null) ? 0 : (int)idState;
                req.Description = (string.IsNullOrEmpty(description))?string.Empty:description;
                List<Tasks> values = await _TaskDa.Get(req);
                if (values.Count == 0)
                {
                    response.Mensaje = "No se Obtuvieron datos";
                    response.Codigo = ResponseEnum.ERROR;
                    response.IdOperacion = "-1";
                }

                else
                {
                    response.Data = values;
                    response.Codigo = ResponseEnum.OK;
                    response.Mensaje = "OK";
                }

            }
            catch (Exception ex)
            {
                response.Codigo = ResponseEnum.FATAL;
                response.Mensaje = ex.Message;
            }

            return response;
        }

        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<ResponseBE<Int64?>> Update(RequestUpdateTask req)
        {
            ResponseBE<Int64?> response = new ResponseBE<Int64?>();
            try
            {
                Int64 value = await _TaskDa.Update(req.Id);
                if (value == null || value != 1)
                {
                    response.Mensaje = "No se pudo actualizar la tarea";
                    response.Codigo = ResponseEnum.ERROR;
                    response.IdOperacion = "-1";
                }

                else
                {
                    response.Data = null;
                    response.Codigo = ResponseEnum.OK;
                    response.Mensaje = "OK";
                }

            }
            catch (Exception ex)
            {
                response.Codigo = ResponseEnum.FATAL;
                response.Mensaje = ex.Message;
            }

            return response;
        }
    }
}
