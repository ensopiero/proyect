﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Enums
{
    public enum ResponseEnum
    {

        OK = 1,

        ERROR = 2,

        FATAL = 3
    }
}
