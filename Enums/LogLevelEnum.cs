﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Enums
{
    public enum LogLevelEnum
    {

        INFORMACION = 1,

        WARNING = 2,

        ERROR = 3,

        FATAL = 4
    }
}
