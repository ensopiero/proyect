﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class Tasks
    {
        public Int64 Id { get; set; }
		public string Description { get; set; }
		public DateTime DateCreation { get; set; }
		public User User { get; set;}
		public State State { get; set; }

	}
}
