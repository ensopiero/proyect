﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models.Requests
{
    public class RequestLogin
    {
        public string UserName { get; set; }
        public string UserPassword { get; set; }
    }
}
