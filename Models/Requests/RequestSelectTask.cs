﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models.Requests
{
    public class RequestSelectTask
    {
        public Int64 Id { get; set; }
        public Int64 IdUser { get; set; }
        public Int64 IdState { get; set; }
        public string Description { get; set; }
    }
}
