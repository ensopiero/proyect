﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models.Requests
{
    public class RequestUpdateTask
    {
        public Int64 Id { get; set; }
    }
}
