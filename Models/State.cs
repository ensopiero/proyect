﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class State
    {
        public Int64 Id { get; set;}
        public string Description { get; set; }
    }
}
