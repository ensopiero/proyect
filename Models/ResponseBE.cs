﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Enums;

namespace Test.Models
{
    public class ResponseBE<T>
    {
        /// <summary>
        /// Constructor para asignar propiedades manualmente
        /// </summary>
        public ResponseBE()
        { }

        /// <summary>
        /// Constructor para respuestas exitosas.
        /// </summary>
        public ResponseBE(T data, string mensaje = "OK", string idOperacion = "0")
            : this(ResponseEnum.OK, mensaje, data, idOperacion)
        { }

        /// <summary>
        /// Constructor para respuestas con código explícito (ej.: error/fatal).
        /// </summary>
        public ResponseBE(ResponseEnum codigo, string mensaje, T data = default(T), string idOperacion = "0")
        {
            Codigo = codigo;
            Mensaje = mensaje;
            Data = data;
            IdOperacion = idOperacion;
        }

        public ResponseEnum Codigo { get; set; }
        public string IdOperacion { get; set; }
        public string Mensaje { get; set; }
        public T Data { get; set; }

    }

    public class ResponseRestBE<TData>
    {
        /// <summary>
        /// Constructor para asignar propiedades manualmente
        /// </summary>
        public ResponseRestBE()
        { }

        /// <summary>
        /// Constructor para respuestas exitosas.
        /// </summary>
        public ResponseRestBE(TData data, string mensaje = "OK")
            : this(CashdroEnum.OK, mensaje, data)
        { }

        /// <summary>
        /// Constructor para respuestas con código explícito (ej.: error/fatal).
        /// </summary>
        public ResponseRestBE(CashdroEnum codigo, string mensaje, TData data = default(TData))
        {
            Codigo = codigo;
            Mensaje = mensaje;
            Data = data;
        }

        public CashdroEnum Codigo { get; set; }

        public string Mensaje { get; set; }

        public TData Data { get; set; }
    }

    public class ResponseRestListBE<TData>
    {
        /// <summary>
        /// Constructor para asignar propiedades manualmente
        /// </summary>
        public ResponseRestListBE()
        { }

        /// <summary>
        /// Constructor para respuestas exitosas.
        /// </summary>
        public ResponseRestListBE(List<TData> data, string mensaje = "OK")
            : this(CashdroEnum.OK, mensaje, data)
        { }

        /// <summary>
        /// Constructor para respuestas con código explícito (ej.: error/fatal).
        /// </summary>
        public ResponseRestListBE(CashdroEnum codigo, string mensaje, List<TData> data = default(List<TData>))
        {
            Codigo = codigo;
            Mensaje = mensaje;
            Data = data;
        }

        public CashdroEnum Codigo { get; set; }

        public string Mensaje { get; set; }

        public List<TData> Data { get; set; }
    }
}


